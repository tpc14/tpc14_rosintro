#! /usr/bin/env python

import sys
import copy
import rospy
import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg

#Initialize moveit_commander and the rospy node
moveit_commander.roscpp_initialize(sys.argv)
rospy.init_node('move_group_python_interface_tutorial', anonymous=True)

#Getting the robot, scene, and group
robot = moveit_commander.RobotCommander()
scene = moveit_commander.PlanningSceneInterface()    
group = moveit_commander.MoveGroupCommander("manipulator")

#Printing out a variety of reference values
print ("Reference frame: %s" % group.get_planning_frame())
print ("End effector: %s" % group.get_end_effector_link())
print ("Robot Groups:")
print (robot.get_group_names())
print ("Current Joint Values:")
print (group.get_current_joint_values())
print ("Current Pose:")
print (group.get_current_pose())
print ("Robot State:")
print (robot.get_current_state())

#Always set the robot to an easily accessible pose to start
#Starting point for the letter T
pose_goal = geometry_msgs.msg.Pose()
pose_goal.orientation.w = 1.0
pose_goal.position.x = 0.5
pose_goal.position.y = 0.3
pose_goal.position.z = 0.3

group.set_pose_target(pose_goal)
# Move robot to starting position
plan = group.go(wait=True)
# Ensures that there is no residual movement
group.stop()
# Clearing targets after planning with poses to ensure no excess movements
group.clear_pose_targets()


#Move end effector to trace a capitalized T
waypoints = []
scale = 1.0

#Move right to create top of T
wpose = group.get_current_pose().pose
wpose.position.y += scale * 0.2  # Move sideways (+y)
waypoints.append(copy.deepcopy(wpose))

#Move back to the center of the top of the T
wpose.position.y -= scale * 0.1  # Second move sideways in (-y)
waypoints.append(copy.deepcopy(wpose))

#Move down to create the stem of the T
wpose.position.z -= scale * 0.2  # Third move downwards in (-z)
waypoints.append(copy.deepcopy(wpose))


# Get the Cartesian path to be interpolated at a resolution of 1 cm
# Hence having 0.01 as the eef_step in Cartesian translation
# Disable the jump threshold by setting it to 0.0,
# ignoring the check for infeasible jumps in joint space
(plan, fraction) = group.compute_cartesian_path(
    waypoints, 0.01, 0.0  # waypoints to follow, eef_step, jump_threshold
)  

#Execute the cartesian waypoint plan
group.execute(plan, wait=True)

# Ensures that there is no residual movement
group.stop()
# Clearing targets after planning with poses to ensure no excess movements
group.clear_pose_targets()


#Start pose for letter P
#Always set the robot to an easily accessible pose to start
pose_goal = geometry_msgs.msg.Pose()
pose_goal.orientation.w = 1.0
pose_goal.position.x = 0.4
pose_goal.position.y = 0.3
pose_goal.position.z = 0.3

group.set_pose_target(pose_goal)

plan = group.go(wait=True)
# Ensures that there is no residual movement
group.stop()
# Clearing targets after planning with poses to ensure no excess movements
group.clear_pose_targets()


#Move end effector to trace a capitalized P
waypoints2 = []
scale2 = 1.0

#Move right to create top of P
wpose = group.get_current_pose().pose
wpose.position.y += scale2 * 0.1  # Move sideways (+y)
waypoints2.append(copy.deepcopy(wpose))

#Move down to create the right end of P
wpose.position.z -= scale2 * 0.05  # Move downwards in (-z)
waypoints2.append(copy.deepcopy(wpose))

#Move left to create bottom of P extrusion
wpose.position.y -= scale2 * 0.1  # Move sideways (-y)
waypoints2.append(copy.deepcopy(wpose))

#Move up to finish top half  of P (connect 1st and 3rd segments)
wpose.position.z += scale2 * 0.05  # Move downwards in (+z)
waypoints2.append(copy.deepcopy(wpose))


#Move down to create the stem of the P
wpose.position.z -= scale2 * 0.2  # Move downwards in (-z)
waypoints2.append(copy.deepcopy(wpose))


# Get the Cartesian path to be interpolated at a resolution of 1 cm
# Hence having 0.01 as the eef_step in Cartesian translation
# Disable the jump threshold by setting it to 0.0,
# ignoring the check for infeasible jumps in joint space
(plan2, fraction) = group.compute_cartesian_path(
    waypoints2, 0.01, 0.0  # waypoints to follow  # eef_step
)  # jump_threshold

#Execute the cartesian waypoint plan
group.execute(plan2, wait=True)

# Ensures that there is no residual movement
group.stop()
# Clearing targets after planning with poses to ensure no excess movements
group.clear_pose_targets()


#Starting position for letter C
pose_goal = geometry_msgs.msg.Pose()
pose_goal.orientation.w = 1.0
pose_goal.position.x = 0.3
pose_goal.position.y = 0.3
pose_goal.position.z = 0.3

group.set_pose_target(pose_goal)

plan = group.go(wait=True)
# Ensures that there is no residual movement
group.stop()
# Clearing targets after planning with poses to ensure no excess movements
group.clear_pose_targets()



#Move end effector to trace a capitalized C
waypoints = []
scale = 1.0

#Move right to create top of C
wpose = group.get_current_pose().pose
wpose.position.y += scale * 0.1  # Move sideways (+y)
waypoints.append(copy.deepcopy(wpose))

#Move left to go back to starting point
wpose.position.y -= scale * 0.1  # Move sideways (-y)
waypoints.append(copy.deepcopy(wpose))

#Move down to create the stem of the C
wpose.position.z -= scale * 0.2  # Third move downwards in (-z)
waypoints.append(copy.deepcopy(wpose))

#Move right to create bottom of C
wpose.position.y += scale * 0.1  # Move sideways (+y)
waypoints.append(copy.deepcopy(wpose))


# Get the Cartesian path to be interpolated at a resolution of 1 cm
# Hence having 0.01 as the eef_step in Cartesian translation
# Disable the jump threshold by setting it to 0.0,
# ignoring the check for infeasible jumps in joint space
(plan, fraction) = group.compute_cartesian_path(
    waypoints, 0.01, 0.0  # waypoints to follow  # eef_step
)  # jump_threshold

#Execute the cartesian waypoint plan
group.execute(plan, wait=True)

# Ensures that there is no residual movement
group.stop()
# Clearing targets after planning with poses to ensure no excess movements
group.clear_pose_targets()

#Finished all letters
#Stop moveit_commander
moveit_commander.roscpp_shutdown()

