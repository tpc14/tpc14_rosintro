import numpy as np
import math


np.set_printoptions(precision=3, suppress=True)

#Get all A and T matrices
def get_matrices(rotations):
    q1 = rotations[0]
    q2 = rotations[1]
    q3 = rotations[2]
    q4 = rotations[3]
    q5 = rotations[4]
    q6 = rotations[5]

    l1 = [q1,.1625,0,np.pi/2]
    l2 = [q2,0,-.425,0]
    l3 = [q3,0,-.3922,0]
    l4 = [q4,.1333,0,np.pi/2]
    l5 = [q5,.0997,0,-np.pi/2]
    l6 = [q6,.0996,0,0]

    a1 = get_matrix(l1)
    a2 = get_matrix(l2)
    a3 = get_matrix(l3)
    a4 = get_matrix(l4)
    a5 = get_matrix(l5)
    a6 = get_matrix(l6)


    result = a1 @ a2 @ a3 @ a4 @ a5 @ a6

    T0 = np.matrix([[1,0, 0,0],
                    [0,1,0,0],
                    [0, 0,1, 0],
                    [0, 0, 0,1]])
    T1 = a1
    T2 = T1 @ a2
    T3 = T2 @ a3
    T4 = T3 @ a4
    T5 = T4 @ a5
    T6 = T5 @ a6

    t_matrices = []
    t_matrices.append(T0)
    t_matrices.append(T1)
    t_matrices.append(T2)
    t_matrices.append(T3)
    t_matrices.append(T4)
    t_matrices.append(T5)
    t_matrices.append(T6)

    print("\nA1")
    print(a1)
    print("\nA2")
    print(a2)
    print("\nA3")
    print(a3)
    print("\nA4")
    print(a4)
    print("\nA5")
    print(a5)
    print("\nA6")
    print(a6)
    print("\nA_final")
    print(result)

    get_jacobian(t_matrices)
    

#Get A_i matrix from theta,d,a,alpha values for a joint
def get_matrix(vars):
    theta = vars[0]
    d = vars[1]
    a = vars[2]
    alpha = vars[3]
    a1 = np.matrix([[math.cos(theta), -math.cos(alpha)*math.sin(theta), math.sin(alpha)*math.sin(theta),a*math.cos(theta)],
                 [math.sin(theta),math.cos(theta)*math.cos(alpha), -math.sin(alpha)*math.cos(theta),a*math.sin(theta)],
                 [0, math.sin(alpha),math.cos(alpha), d],
                 [0, 0, 0,1]])
    return a1
#Get the jacobian matrix given the T matrices
def get_jacobian(t_matrices):
    jacobian = np.matrix(np.zeros((6, 6)))
    for i in range(1,len(t_matrices)):
        # fill in top half of jacobian
        jacobian[:3,i-1] = np.cross(t_matrices[i-1][:3,2], (t_matrices[6][:3,3] - t_matrices[i-1][:3,3]),axis=0)
        # fill in bottom half of jacobian
        jacobian[3:,i-1] = t_matrices[i-1][:3,2]
    print("\nJacobian")
    print(jacobian)

#All joint angles for all 3 poses
pose_1 = [0.4090007597168084, -1.0664842578222764, 1.7773604009769297, 2.4298384204587595, -1.9788138850752226, -0.0009261938206339693]
pose_2 = [0.7064510393195658, -0.79510938955067, 1.292865207288413, 2.6439471419436895, -2.2770745234617342, 0.0009612063436703622]
pose_3 = [0.5740257085930569, -0.9446435508231037, 1.565705443314358, 2.519581305504369, -2.1448759917130067, -0.000426595845040012]


#Running the funtions to get A matrices, T matrices, and Jacobians
get_matrices(pose_1)
get_matrices(pose_2)
get_matrices(pose_3)
